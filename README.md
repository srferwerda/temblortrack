TemblorTrack

Assignment:

Using the API provided by the USGS Earthquake Hazards Program (http://ehp2-earthquake.wr.usgs.gov/fdsnws/event/1/), create an app that presents a map view to the user with recent earthquake events called out. Details and history would be cool too. You will be judged on beauty, poise and personality. In all seriousness - show us your creativity and your ability to produce top-notch professional software and not just the ability to get it to work.

Credits:
App icon "seismic-graph" made by Freepik from www.flaticon.com.